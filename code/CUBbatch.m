rawdata = zeros(length(trainlist),height*width);
for f = 1 : length(trainlist)
    mask = imread([maskpath trainlist{f} '.png']);
    mask = imresize(mask,0.5);
    mask = double(mask>0);
    rawdata(f,:) = double(mask(:));
end

% create mini batches
numbatches = floor(size(rawdata,1)/batchsize);
batchdata = zeros(batchsize, size(rawdata,2), numbatches);
randomorder = randperm(size(rawdata,1));
for b=1:numbatches
    batchdata(:,:,b) = rawdata(randomorder(1+(b-1)*batchsize:b*batchsize), :);
end;

save([datadir 'Birds.mat'], 'rawdata', 'batchdata');