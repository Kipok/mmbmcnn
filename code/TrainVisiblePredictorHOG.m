% PFP pre-train hidfea

list = trainlist;

mask = imread([maskpath list{1} '.png']);
mask = imresize(mask,[height, width]);
mask = mask(:);
load([hogpath list{1} '.mat']);
hog = hog(:);
    
data = zeros(length(list),length(hog));
label = zeros(length(list),length(mask));

for f = 1 : length(list)
    mask = imread([maskpath list{f} '.png']);
    mask = mask > 0;
    mask = imresize(mask,[height, width]);
    label(f,:) = double(mask(:));
    load([hogpath list{f} '.mat']);
    data(f,:) = hog(:);
end

hogvis = zeros(size(data,2)+1,length(mask(:)));
matlabpool 4
parfor c = 1 : size(hogvis,2)
    c
    model = train(label(:,c), sparse(data), '-s 0 -B 1 -q 1');
    predict(label(:,c), sparse(data), model);
    if model.Label(1)==0
        hogvis(:,c) = -model.w';
    else
        hogvis(:,c) = model.w';
    end
%     template = reshape(hogvis(1:end-1,c),size(hog));
%     imhog = vl_hog('render', single(template.*(template>0))) ;
%     clf ; imshow(imhog,[]) ; colormap gray ; drawnow;
end
matlabpool close
save(modelstr_hogvis, 'hogvis');
