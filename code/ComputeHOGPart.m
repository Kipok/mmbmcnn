% compute HOG on image parts

list = imglist;
for f = 1:numel(list)
    
    f
%     outfile = ['../data/tmp/PFP/pedHOGs/' list{f} '.mat']
    [pathstr, name, ext] = fileparts(list{f});
    if ~isempty(pathstr)
        outstr = [partpath pathstr '/'];
        if ~isdir(outstr)
            mkdir(outstr);
        end
    else
        outstr = partpath;
    end
    outfile = [outstr name '.mat'];
    if exist(outfile, 'file')
       continue;
    end

    im = imread([imgpath list{f} '.png']);
    
    hogparts = [];
    for k = 1:size(visindices,2);
        mask = reshape(visindices(:,k),height,width);
        mask = (imresize(mask,scaling)>0.5);
        xind = find(sum(mask,1));
        x1 = xind(1); x2 = xind(end);
        yind = find(sum(mask,2));
        y1 = yind(1); y2 = yind(end);
        cellSize = 8 ;
        hog = vl_hog(single(im(y1:y2,x1:x2,:)), cellSize) ;
        hogparts = [hogparts, hog(:)];
    end

    save(outfile, 'hogparts');
    
end
