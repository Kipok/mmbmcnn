% compute image HOG

list = imglist;
for f = 1:numel(list)
    
%     outfile = ['../data/tmp/PFP/pedHOGs/' list{f} '.mat']
    [pathstr, name, ext] = fileparts(list{f});
    outstr = [hogpath pathstr '/'];
    if ~isdir(outstr)
        mkdir(outstr);
    end
    
    outfile = [outstr name '.mat'];
    if exist(outfile, 'file')
       continue;
    end

    im = imread([imgpath list{f} '.png']);
    cellSize = 8 ;
    hog = vl_hog(single(im), cellSize, 'verbose') ;


    save(outfile, 'hog');
    
end
