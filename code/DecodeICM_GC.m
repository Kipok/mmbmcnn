% Decoding RBM by ICM (iterated conditional modes) with Graph Cut

iter = iter_rbm;
load([modelpath_combinedloss 'model_iter' num2str(iter) '.mat']);

list = testlist;

gt = cell(length(list),1);
y = cell(length(list),1);

for f = 1 : length(list)
    
    im = imread([imgpath list{f} '.png']);
    load([segpath list{f} '_t' num2str(threshold) '.mat']);
    load([feapath list{f} '.mat']);
    load([hogpath list{f} '.mat']);
    mask = imread([maskpath list{f} '.png']);
    gt{f}= mask(:)>0;
    
    predicthid = [hog(:); 1]'*feahid;
    predictvis = [hog(:); 1]'*feavis;
    feascore = reshape(predictvis, height, width);
    feascore = imresize(feascore, scaling);
    
    
    probhid = 1 ./ (1 + exp(-predicthid));
    hid0 = (probhid > 0.5);

    % overall, hog + superpixel
    [vis, hid] = decode_blp(vishid, visbiases+predictvis, hidbiases+predicthid, 100, [], hid0);
    hidscore = hid*vishid' + visbiases;
    hidscore = reshape(hidscore,[height,width]);
    hidscore = imresize(hidscore,scaling);

    probmask = 1 ./ (1 + exp(- hidscore - feascore));
    predictmask = cut_with_prior(probmask, im2double(im), lambda);
    y{f} = predictmask(:);
    
    [pathstr, name, ext] = fileparts(list{f});
    resstr = [respath_rbmgc pathstr '/'];
    if ~isdir(resstr)
        mkdir(resstr);
    end
    
    imwrite(uint8(255*predictmask), [respath_rbmgc list{f} '_gc.png'], 'png');

    mask = cat(3, 0.5*predictmask, 0.0*ones(size(predictmask)), 0.0*ones(size(predictmask)));
    im_mask = im2double(im) + double(mask);
    
    im_mask = uint8(255*im_mask);
    imwrite(im_mask, [respath_rbmgc list{f} '_gc_overlay.png'], 'png');
end

fprintf('=========================\n');
gt = cell2mat(gt);
y  = cell2mat(y);

foreground = find(gt==1);
background = find(gt==0);

foreaccuracy = sum(y(foreground)==1)/length(foreground);
backaccuracy = sum(y(background)==0)/length(background);
avg = (foreaccuracy + backaccuracy) / 2
tp = sum((gt==1) & (y==1));
fp = sum((gt~=1) & (y==1));
fn = sum((gt==1) & (y~=1));
tn = sum((gt~=1) & (y~=1));
overlap = tp / (tp+fp+fn)

fid = fopen('../data/rbmgc_res.txt', 'w');
fprintf(fid, 'RBM with Graph Cut\n=========================\nforeaccuracy = %.4f\nbackaccuracy = %.4f\naverage = %.4f\noverlap = %.4f\n', ...
        foreaccuracy, backaccuracy, avg, overlap);
fprintf(fid, '=========================\niteration = %d\n', iter);
