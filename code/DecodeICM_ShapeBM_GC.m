% Decoding RBM by ICM (iterated conditional modes) with Graph Cut

iter = iter_shapebm;
load([modelpath_shapbm_combinedloss 'model_iter' num2str(iter) '.mat']);


gt = cell(length(list),1);
y = cell(length(list),1);

for f = 1 : length(list)
    
    load([segpath list{f} '_t' num2str(threshold) '.mat']);
    load([feapath list{f} '.mat']);
    load([hogpath list{f} '.mat']);
    load([partpath list{f} '.mat']);
    mask = imread([maskpath list{f} '.png']);
    im = imread([imgpath list{f} '.png']);
    gt{f}= uint8(mask(:)>0);
    
    predictpen = [hog(:); 1]'*feapen;
    
    predicthid = zeros(size(hidbiases));
    for k = 1:4
        predicthid(hidindices(:,k)>0) = [hogparts(:,k);1]'*feahid(:,hidindices(:,k)>0);
    end
    
    predictvis = [hog(:); 1]'*feavis;
    feascore = reshape(predictvis, height, width);
    feascore = imresize(feascore, scaling);
    
    probhid = 1 ./ (1 + exp(-predicthid));
    hid0 = (probhid > 0.5);

    % overall, hog + superpixel
    [vis, hid, pen] = decode_model_blp(vishid_tilde, visbiases+predictvis, hidbiases+predicthid, ...
        hidpen, penbiases+predictpen, 100, hid0);
    
    hidscore = hid*vishid_tilde' + visbiases;
    hidscore = reshape(hidscore,[height,width]);
    hidscore = imresize(hidscore,scaling);

    probvis = 1 ./ (1 + exp(- hidscore - feascore));
    predictmask = cut_with_prior(probvis, im2double(im), lambda);
    y{f} = predictmask(:);
    
    [pathstr, name, ext] = fileparts(list{f});
    resstr = [respath_shapebmgc pathstr '/'];
    if ~isdir(resstr)
        mkdir(resstr);
    end
    
    imwrite(uint8(255*predictmask), [respath_shapebmgc list{f} '_gc2.png'], 'png');

    mask = cat(3, 0.5*predictmask, 0.0*ones(size(predictmask)), 0.0*ones(size(predictmask)));
    im_mask = im2double(im) + double(mask);
    
    im_mask = uint8(255*im_mask);
    imwrite(im_mask, [respath_shapebmgc list{f} '_gc2_overlay.png'], 'png');
end


fprintf('=========================\n');
gt = cell2mat(gt);
y  = cell2mat(y);

foreground = find(gt==1);
background = find(gt==0);

foreaccuracy = sum(y(foreground)==1)/length(foreground);
backaccuracy = sum(y(background)==0)/length(background);
avg = (foreaccuracy + backaccuracy) / 2
tp = sum((gt==1) & (y==1));
fp = sum((gt~=1) & (y==1));
fn = sum((gt==1) & (y~=1));
tn = sum((gt~=1) & (y~=1));
overlap = tp / (tp+fp+fn)

fid = fopen('../data/shapebmgc_res.txt', 'w');
fprintf(fid, 'ShapeBM with Graph Cut\n=========================\nforeaccuracy = %.4f\nbackaccuracy = %.4f\naverage = %.4f\noverlap = %.4f\n', ...
        foreaccuracy, backaccuracy, avg, overlap);
fprintf(fid, '=========================\niteration = %d\n', iter);