%compute regions (superpixels) by thresholding ucms

for f = 1:numel(imglist)
    
    [pathstr, name, ext] = fileparts(imglist{f});
    segstr = [segpath pathstr '/'];
    if ~isdir(segstr)
        mkdir(segstr);
    end
    
    segfile = [segstr name '_t' num2str(threshold) '.mat'];
    if exist(segfile,'file')
        continue;
    end
    
    load([ucmpath imglist{f} '.mat']);
    ucm(ucm<threshold) = 0;
    [regions, nRegions, Tree, V] = treeBuild(ucm);
%     regions = bwlabel(ucm==0);
    save(segfile,'regions', 'nRegions', 'Tree', 'V');
    fprintf('Process the image %s\n', imglist{f});
end

% for f = 1 : numel(imglist)
%     
%     load([ucmpath imglist{f} '.mat']);
%     ucm(ucm<thres) = 0;
%     [regions, nRegions, Tree, V] = pylonBuild(ucm);
% %     regions = bwlabel(ucm==0);
%     save([segpath imglist{f} '_t' num2str(thres) '.mat'],'regions', 'nRegions', 'Tree', 'V');
%     fprintf('Process the image %s\n', imglist{f});
% end

