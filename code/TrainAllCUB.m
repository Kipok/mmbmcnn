% train cub mmbms for segmentation

% -------- prepare data ---------------------
% CUBcrop;
% CUBsplit;
% 
% % % ------ config for training --------------
% AddPath;
% CUBconfig;
% 
% % -------- extract features ------------------
% ComputeUCM;
% ComputeRegions;
% ComputeFeatures;
% ComputeHOG;
% ComputeHOGPart;
% 
% % -------- train RBMs -------------------
% CUBbatch;
% batch1 = load([datadir 'Birds.mat']);
% batchdata = batch1.batchdata;
% TrainRBM;
% TrainShapeBM;
% 
% % ------- train Max-Margin BMs --------------
% TrainVisiblePredictorHOG;
% InferHiddenStates_RBM;
% TrainHiddenPredictor_RBM;
% InferHiddenStates_ShapeBM;
% TrainHiddenPredictor_ShapeBM;

iter_rbm = 0;
iter_shapebm = 0;

% TrainDiscRBMCombinedLoss;
% TrainDiscShapeBMCombinedLoss;

% -------- test Max-Margin BMs -------------

DecodeICM;
DecodeICM_GC;
DecodeICM_ShapeBM;
DecodeICM_ShapeBM_GC;
