function [hid, pen] = decode_data_blp(vishid, visbias, hidbias, hidpen, penbias, BLPIters, vis0)


vis = vis0;
pen = double(rand(size(penbias))>0.5);
for i = 1 : BLPIters
    func = vis*vishid + hidbias + pen*hidpen';
    hid = double(func > 0);
    fval = hid*func' + penbias*pen';

    func_new = hid*hidpen + penbias;
    pen = double(func_new > 0);
    fval_new = func_new*pen' + vis*vishid*hid' + hidbias*hid';
    if abs(fval-fval_new) < 1e-3
        break;
    end
end