% PFP pre-train hidfea

list = trainlist;
load([hidpath list{1} '.mat']);
load([hogpath list{1} '.mat']);
hog = hog(:);
    
data = zeros(length(list),length(hog));
label = zeros(length(list),length(pen));
for f = 1 : length(list)
    load([hidpath list{f} '.mat']);
    label(f,:) = double(pen);
    load([hogpath list{f} '.mat']);
    data(f,:) = hog(:);
end

feapen = zeros(size(data,2),length(pen));
feapenbiases = zeros(1,length(pen));
% matlabpool 4
for c = 1 : length(pen)
    c
    model = train(label(:,c), sparse(data), '-s 0 -B 1 -q 1');
    predict(label(:,c), sparse(data), model);
    if model.Label(1)==0
        feapen(:,c) = -model.w(:,1:end-1)';
        feapenbiases(c) = -model.w(end);
    else
        feapen(:,c) = model.w(:,1:end-1)';
        feapenbiases(c) = model.w(end);
    end
    
%     template = reshape(feapen(:,c),size(hog));
%     imhog = vl_hog('render', single(template.*(template>0))) ;
%     clf ; imshow(imhog,[]) ; colormap gray ; drawnow;
end
clear data label

%% ----------------------------------------------------------------------
data = cell(length(list),1);
label = zeros(length(list),length(hid));
for f = 1 : length(list)
    load([hid2path list{f} '.mat']);
    label(f,:) = double(hid);
    load([partpath list{f} '.mat']);
    data{f} = hogparts;
end

feahid = zeros(size(data{1},1),length(hid));
feahidbiases = zeros(1,length(hid));
for k = 1 : 4
    k
    xx = zeros(length(list),size(data{1},1));
    for f = 1:length(list)
        xx(f,:) = data{f}(:,k);
    end
    subset = find(hidindices(:,k));
    for c = 1 : length(subset)
        cid  = subset(c);
        model = train(label(:,cid), sparse(xx), '-s 0 -B 1 -q 1');
        predict(label(:,cid), sparse(xx), model);
        if model.Label(1)==0
            feahid(:,cid) = -model.w(:,1:end-1)';
            feahidbiases(cid) = -model.w(end);
        else
            feahid(:,cid) = model.w(:,1:end-1)';
            feahidbiases(cid) = model.w(end);
        end
    end
end
save(modelstr_feahid2, 'feapen', 'feapenbiases', 'feahid', 'feahidbiases');
