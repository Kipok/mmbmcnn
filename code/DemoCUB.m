% Demo for segmenting birds
AddPath;
CUBconfig;

load(siftvocabpath);
load(colorvocabpath);
load(modelstr_mmbm1);

for f = 1 : numel(testlist)
    
    name = testlist{f};
    % imresize
    im = imread([imgpath name '.png']);
    im = imresize(im, [hmax, NaN]);  % keep aspect ratio for now
    orig_w = size(im, 2);
    padded_img = padarray(im, [0 wmax] ,'replicate'); % 24 on both sides, should be enough
    middle_col = floor(size(padded_img, 2) / 2); % we'll copy around this
    im = padded_img(:, (middle_col - wmax/2) : (middle_col + wmax/2 - 1), :);
    
    % compute hog features
    hog = vl_hog(single(im), cellSize);

    fprintf('segmentation\n');    
    % compute gPb segmentation
    im_ = double(im)/255;
    [gPb_orient, gPb_thin, textons] = globalPb_im(im_);
    ucm = contours2ucm(gPb_orient);
    ucm = uint8(255.*ucm);
    ucm(ucm<threshold) = 0;
    [regions, nRegions, Tree, V] = treeBuild(ucm);
    
    fprintf('features\n');
    %computing word histograms for base regions
    [frames, sifts] = vl_phow(single(rgb2gray(im)),'Step',4);
    [ix, w] = LLCEncode(single(sifts), single(siftVocab), llcK);          
    loc = sub2ind([size(im,1) size(im,2)], round(frames(2,:)'), round(frames(1,:)'));
    smpRegions = regions(loc);
    smpRegions = repmat(smpRegions', [llcK 1]);
    wordIndex = (smpRegions-1)*size(siftVocab,2)+ix;
    wordVec = accumarray(wordIndex(:),w(:),[nRegions*size(siftVocab,2) 1]);
    wordHist = reshape(wordVec,[size(siftVocab,2) nRegions]);
    wordHist = single(wordHist);

    %computing color histograms for base regions
    colors = reshape(shiftdim(im,2),[3 size(im,1)*size(im,2)]);
    colors = vl_ikmeanspush(colors,colorVocab);
    colorHist = accumarray([colors(:) regions(:)],...
       ones(numel(colors),1), [size(colorVocab,2) nRegions]); 
    colorHist = single(colorHist);

    %computing shape histograms for regions
    shapeHist = zeros(4*4*size(gPb_orient,3),nRegions,'single');  
    regionMask = cell(nRegions,1);
    for i = 1:nRegions
       regionMask{i} = find(regions == i);
       shapeHist(:,i) = compute_shape_hist(regionMask{i},gPb_orient);
    end

    %L1-normalization for subsequent homkermapping
    wordHist(wordHist < 0) = 0;
    wordHist = bsxfun(@times,wordHist,1.0./(sum(wordHist)+1e-10));
    colorHist = bsxfun(@times,colorHist,1.0./(sum(colorHist)+1e-10));
    shapeHist = bsxfun(@times,shapeHist,1.0./(sum(shapeHist)+1e-10));
    
    fprintf('inference\n');
    % RBM inference
    predicthid = [hog(:); 1]'*feahid;
    
    ft = vl_homkermap([wordHist; colorHist; shapeHist], 1, 'Gamma', 0.7);
    feascore = feavis*[ft; ones(1,size(ft,2))];
    feascore = feascore(regions);
    predictvis = imresize(feascore, [height,width]);
    predictvis = predictvis(:)';
    
    probhid = 1 ./ (1 + exp(-predicthid));
    hid0 = (probhid > 0.5);

    % overall, hog + superpixel
    [vis, hid] = decode_blp(vishid, visbiases+predictvis, hidbiases+predicthid, 100, [], hid0);
    hidscore = hid*vishid' + visbiases;
    hidscore = reshape(hidscore,[height,width]);
    hidscore = imresize(hidscore,scaling);
    hidscore_super = zeros(size(hidscore));
    for r = 1 : length(unique(regions))
        s = hidscore(regions==r);
        s = mean(s);
        hidscore_super(regions==r) = s;
    end
    probvis = 1 ./ (1 + exp(- hidscore_super - feascore));
    
    % graph cut inference
    im = im2double(im);
    predictmask_gc = cut_with_prior(probvis, im, lambda); 

    mask = cat(3, 0.5*predictmask_gc, 0.0*ones(size(predictmask_gc)), 0.0*ones(size(predictmask_gc)));
    im_mask = im + double(mask);
    im_mask = uint8(255*im_mask);
    
    figure; subplot(1,3,1); imshow(im);
    subplot(1,3,2); imshow(predictmask_gc);
    subplot(1,3,3); imshow(im_mask);
    pause
    
end