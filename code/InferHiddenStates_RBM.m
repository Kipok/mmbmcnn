% Infer hidden states

list = trainlist;
load(modelstr_rbm);

for f = 1 : length(list)
    mask = imread([maskpath list{f} '.png']);
    mask = imresize(mask, [height, width]);
    data = double(mask(:)'>0);
    probhid  = 1 ./ (1 + exp(-data*(vishid) - hidbiases));
    hid = probhid > 0.5;
    
    [pathstr, name, ext] = fileparts(list{f});
    hidstr = [hidpath pathstr '/'];
    if ~isdir(hidstr)
        mkdir(hidstr);
    end
    
    save([hidpath list{f} '.mat'], 'hid');
end

