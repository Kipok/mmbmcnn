function [vis, hid, pen] = decode_model_blp(vishid, visbias, hidbias, hidpen, penbias, BLPIters, hid0)

if isempty(hid0)
    hid = double(rand(size(hidbias))>0.5);
else
    hid = hid0;
end
for i = 1 : BLPIters

    func = hid*vishid' + visbias;
    vis = double(func > 0);

    func_1 = hid*hidpen + penbias;
    pen = double(func_1 > 0);
    fval_1 = func_1*pen' + vis*vishid*hid' + hidbias*hid' + visbias*vis';

    func_2 = vis*vishid + hidbias + pen*hidpen';
    hid = double(func_2 > 0);
    fval_2 = hid*func_2' + visbias*vis' + penbias*pen';

    if abs(fval_1-fval_2) < 1e-3
        break;
    end
end