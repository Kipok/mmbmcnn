% hidden partition
tp = numpatch(1)*numpatch(2);
hidindices = kron(eye(tp,tp),ones(numhid/tp,1));
numhidcell = numhid/tp;

% visible partition
visindices = zeros(height*width,4);
p=1;
for i=1:numpatch(1)
    for j=1:numpatch(2)
        mask = zeros(height,width);
        mask((1:cellheight)+(j-1)*stride(2),(1:cellwidth)+(i-1)*stride(1)) = ones(cellheight,cellwidth);
        visindices(:,p) = mask(:);
        p=p+1;
    end
end

numviscell=cellheight*cellwidth;

