function vishid_tilde = convertpatch2image(vishid,visindices,hidindices)

[~,numhidcell] = size(vishid);
[numdims,~] = size(visindices);
[numhid,~] = size(hidindices);
vishid_tilde = zeros(numdims,numhid);
for p = 1:4
    vind = find(visindices(:,p));
    hind = (p-1)*numhidcell+(1:numhidcell);
    [xx,yy] = meshgrid(hind,vind);
    ind = sub2ind([numdims,numhid],yy(:),xx(:));
    vishid_tilde(ind) = vishid(:);
end