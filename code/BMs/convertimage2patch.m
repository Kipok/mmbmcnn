function vishid = convertimage2patch(vishid_tilde,visindices,hidindices)

numviscell = length(find(visindices(:,1)));
numhidcell = length(find(hidindices(:,1)));
vishid = zeros(numviscell,numhidcell);
for p = 1 : 4
    vind = find(visindices(:,p));
    hind = find(hidindices(:,p));
    for i = 1:numhidcell
        vishid(:,i) = vishid(:,i) + vishid_tilde(vind,hind(i));
    end
end