% extract superpixel features

load(siftvocabpath,'siftVocab');
load(colorvocabpath,'colorVocab');

list = imglist;

for f = 1:numel(list)
    
    [pathstr, name, ext] = fileparts(list{f});
    feastr = [feapath pathstr '/'];
    if ~isdir(feastr)
        mkdir(feastr);
    end
    
    feafile = [feastr name '.mat']
    if exist(feafile,'file')
        continue;
    end
    
%     feafile = ['../data/tmp/PFP/pedFeatures/' list{f} '.mat']
%     if exist(feafile, 'file')
%        continue;
%     end

    labels = imread([maskpath list{f} '.png']);
    labels = labels > 0;
    labels = double(2-labels); % 1 for foreground, 2 for background
    im = imread([imgpath list{f} '.png']);
    load([ucmpath list{f} '.mat']);
    load([segpath list{f} '_t' num2str(threshold) '.mat']);
    nRegions = length(unique(regions));
    

    %computing word histograms for base regions
    [frames sifts] = vl_phow(single(rgb2gray(im)),'Step',4);
    [ix w] = LLCEncode(single(sifts), single(siftVocab), llcK);          
    loc = sub2ind([size(im,1) size(im,2)], round(frames(2,:)'), round(frames(1,:)'));
    smpRegions = regions(loc);
    smpRegions = repmat(smpRegions', [llcK 1]);
    wordIndex = (smpRegions-1)*size(siftVocab,2)+ix;
    wordVec = accumarray(wordIndex(:),w(:),[nRegions*size(siftVocab,2) 1]);
    wordHist = reshape(wordVec,[size(siftVocab,2) nRegions]);
    wordHist = single(wordHist);

    %computing color histograms for base regions
    colors = reshape(shiftdim(im,2),[3 size(im,1)*size(im,2)]);
    colors = vl_ikmeanspush(colors,colorVocab);
    colorHist = accumarray([colors(:) regions(:)],...
       ones(numel(colors),1), [size(colorVocab,2) nRegions]); 
    colorHist = single(colorHist);

    %computing label histograms for base regions
    labelIndex = (regions-1)*nLabels+labels;      
    labelHist = accumarray(labelIndex(:),ones(numel(labelIndex),1),[nRegions*nLabels 1]);      
    labelHist = reshape(labelHist,[nLabels nRegions]);
    labelHist = single(labelHist);

    %computing shape histograms for regions
    shapeHist = zeros(4*4*size(gPb_orient,3),nRegions,'single');  
    regionMask = cell(nRegions,1);
    for i = 1:nRegions
       regionMask{i} = find(regions == i);
       shapeHist(:,i) = compute_shape_hist(regionMask{i},gPb_orient);
    end

    %L1-normalization for subsequent homkermapping
    wordHist(wordHist < 0) = 0;
    wordHist = bsxfun(@times,wordHist,1.0./(sum(wordHist)+1e-10));
    colorHist = bsxfun(@times,colorHist,1.0./(sum(colorHist)+1e-10));
    shapeHist = bsxfun(@times,shapeHist,1.0./(sum(shapeHist)+1e-10));

    save(feafile, 'wordHist', 'colorHist', 'shapeHist', 'labelHist');
    
end
