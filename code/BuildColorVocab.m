% train color vacabulary
list = trainlist;
N = length(list);
K = 100;

sample = vl_colsubset(1:numel(list),N);
all_colors = zeros(3, N*K, 'uint8');

for i = 1:numel(sample)
    i
    im = imread(fullfile(imgpath,[list{sample(i)},'.png']));
    colors = reshape(shiftdim(im,2),[3 size(im,1)*size(im,2)]);
    all_colors(:,(i-1)*K+1:i*K) = vl_colsubset(colors,K); 
end

colorVocab = vl_ikmeans(all_colors,sz_color);
save(colorvocabpath,'colorVocab');