function shapeHist = compute_shape_hist(pixels, gPb_orient)

[y x] = ind2sub([size(gPb_orient,1) size(gPb_orient,2)],pixels);

miny = min(y);
minx = min(x);
maxy = max(y);
maxx = max(x);

mask = zeros(maxy-miny+1,maxx-minx+1);
mask( sub2ind(size(mask), y-miny+1, x-minx+1) ) = 1;
shapeHist = compute_bwo_pb(gPb_orient(miny:maxy, minx:maxx,:), mask, 4, 4);
shapeHist = shapeHist(:);

