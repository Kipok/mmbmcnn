% max margin training with stochastic gradient descent
% written by Jimei Yang @UCM, May 2013
warning off
    
load(modelstr_rbm);
load(modelstr_feavis);
load(modelstr_feahid);

feahid = [feahid; feahidbiases];
feahid = rand(size(feahid))-0.5;
feavis = [feavis feavisbias]; 
feavis = rand(size(feavis))-0.5;

% parameters 
rho = 1e-3;
c = 1e-2;
CCCPIterations = 50;
t0 = CCCPIterations/5;
t = 0;

% start training
for i = 1 : CCCPIterations
    
    tic;
    
    % Take a random permutation of the samples
    ind_rnd = randperm(length(trainlist));
    
    % update the learning rate
    t = t + 1;
    rho_i = min(rho, rho*t0/t);
    
    fprintf('Train Discriminative RBM without Margin,  Loop = %d ', i);
    
    for batch = 1 : length(trainlist)
        
        if mod(batch,100)==0
            fprintf('.');
        end
        
        bIdx = ind_rnd(batch);
        
        % load the data
        id = trainlist{bIdx};
        load([hogpath id '.mat']);
        load([feapath id '.mat']);
        load([segpath id '_t' num2str(threshold) '.mat']);
        gt = imread([maskpath id '.png']);
        gt = gt > 0;
        
        % compute the image dependent energy
        predicthid = [hog(:); 1]'*feahid;
        ft = vl_homkermap([wordHist; colorHist; shapeHist], 1, 'Gamma', 0.7);
        feascore = feavis*[ft; ones(1,size(ft,2))];
        feascore = feascore(regions);
        predictvis = imresize(feascore, [height,width]);
        predictvis = predictvis(:)';
        
        % infer hidden states given visibles
        mask = imresize(gt, [height, width]);
        vis_star = double(mask(:)');
        hid_star = (vis_star*vishid + hidbiases + predicthid) > 0;
        
        % loss augmented energy minimization
        hidbiastotal = hidbiases + predicthid;
        visbiastotal = visbiases + predictvis;
        [vis, hid] = decode_icm(vishid, visbiastotal, hidbiastotal, 100, [], []);
        
        % compute features for data part
        vishidinc_data = vis_star'*hid_star;
        visbiasinc_data = vis_star;
        hidbiasinc_data = hid_star;
        feahidinc_data = [hog(:);1]*hid_star;
        vissuper_star = imresize(reshape(vis_star,[height,width]),size(gt));
        visregion_star = zeros(size(ft,2),1);
        for r = 1 : length(unique(regions))
            s = vissuper_star(regions==r);
            visregion_star(r) = (mean(s)>0.5);
        end
        feavisinc_data = visregion_star'*[ft; ones(1,size(ft,2))]';
        
        % compute features for model part
        vishidinc_model = vis'*hid;
        visbiasinc_model = vis;
        hidbiasinc_model = hid;
        feahidinc_model = [hog(:);1]*hid;
        vissuper = imresize(reshape(vis,[height,width]),size(gt));
        visregion = zeros(size(ft,2),1);
        for r = 1 : length(unique(regions))
            s = vissuper(regions==r);
            visregion(r) = (mean(s)>0.5);
        end
        feavisinc_model = visregion'*[ft; ones(1,size(ft,2))]';
        
        % compute the gradients
        vishidinc = c*vishid + (vishidinc_model - vishidinc_data);
        visbiasinc = c*visbiases + (visbiasinc_model - visbiasinc_data);
        hidbiasinc = c*hidbiases + (hidbiasinc_model - hidbiasinc_data);
        feahidinc = c*feahid + (feahidinc_model - feahidinc_data);
        feavisinc = c*feavis + (feavisinc_model - feavisinc_data);
    
        % update parameters
        vishid = vishid - rho_i*vishidinc;
        visbiases = visbiases - rho_i*visbiasinc;
        hidbiases = hidbiases - rho_i*hidbiasinc;
        feahid = feahid - rho_i*feahidinc;
        feavis = feavis - rho_i*feavisinc;

    end
    
    list = trainlist; EvaluateDiscRBM; trainAcc = avg;
    list = testlist; EvaluateDiscRBM; testAcc = avg;
    % Show progress in epoch
    fprintf(' TrainAcc = %f, TestAcc = %f, Elapsed Time = %f\n', trainAcc, testAcc, toc);
    
    save([modeldir_percloss 'model_iter' num2str(i) '.mat'],'vishid', 'visbiases', 'hidbiases', 'feahid', 'feavis');
    
end
    

