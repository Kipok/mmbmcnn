% compute UCM

tic
matlabpool 4
parfor f = 1:length(imglist)
% for f = 1:length(imglist)
    
    [pathstr, name, ext] = fileparts(imglist{f});
    ucmstr = [ucmpath pathstr '/'];
    if ~isdir(ucmstr)
        mkdir(ucmstr);
    end
    
    ucmfile = [ucmstr name '.mat'];
    if exist(ucmfile,'file')
        continue;
    end
%     load([ucmpath imglist{f-1} '.mat']);
%     ucm = flipdim(ucm,2);
%     gPb_orient = flipdim(gPb_orient,2);
%     gPb_thin = flipdim(gPb_thin,2);
%     textons = flipdim(textons,2);
%     save(ucmfile, 'ucm', 'gPb_orient', 'gPb_thin', 'textons');
    im = imread([imgpath, imglist{f} '.png']);
    im = double(im)/255;
    [gPb_orient, gPb_thin, textons] = globalPb_im(im);
    ucm = contours2ucm(gPb_orient);
    ucm = uint8(255.*ucm);
    
    SaveUCM(ucmfile, ucm, gPb_orient, gPb_thin, textons);
    
    fprintf('Processing %d image in %f seconds.\n', f, toc);
end
matlabpool close

% for f = 1:numel(imglist)
%     
%     ucmfile = [ucmpath name '.mat'];
%     if exist(ucmfile,'file')
%         continue;
%     end
% %     load([ucmpath imglist{f-1} '.mat']);
% %     ucm = flipdim(ucm,2);
% %     gPb_orient = flipdim(gPb_orient,2);
% %     gPb_thin = flipdim(gPb_thin,2);
% %     textons = flipdim(textons,2);
% %     save(ucmfile, 'ucm', 'gPb_orient', 'gPb_thin', 'textons');
%     im = imread([imgpath, imglist{f} '.png']);
%     im = double(im)/255;
%     [gPb_orient, gPb_thin, textons] = globalPb_im(im);
%     ucm = contours2ucm(gPb_orient);
%     ucm = uint8(255.*ucm);
%     
%     SaveUCM(ucmfile, ucm, gPb_orient, gPb_thin, textons);
%     
%     fprintf('Processing %d image in %f seconds.\n', f, toc);
% end
