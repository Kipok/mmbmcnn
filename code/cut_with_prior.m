function mask = cut_with_prior(probvis, im, lambda)

if nargin < 3
    lambda = 10;
end

height = size(im, 1);
width = size(im, 2);

probvis_im = imresize(probvis, [height width], 'bilinear');

[Hc1 Vc1] = gradient(imfilter(squeeze(im(:,:,1)),fspecial('gauss',[3 3]),'symmetric'));
[Hc2 Vc2] = gradient(imfilter(squeeze(im(:,:,2)),fspecial('gauss',[3 3]),'symmetric'));
[Hc3 Vc3] = gradient(imfilter(squeeze(im(:,:,3)),fspecial('gauss',[3 3]),'symmetric'));
Hc = Hc1.^2+Hc2.^2+Hc3.^2;
Vc = Vc1.^2+Vc2.^2+Vc3.^2;
sigma = mean([Hc(:).^.5;Vc(:).^.5]);

% "data cost", unary costs for pixels per label
unary_maps(:,:,1) = -log(probvis_im+eps);
unary_maps(:,:,2) = -log(1-probvis_im+eps);

% 2 labels, uniform cost if they differ
smoothness_cost = [0 1; 1 0] * lambda;

gch = GraphCut('open', unary_maps, smoothness_cost, single(exp(-.5*Vc/(sigma^2))), single(exp(-.5*Hc/(sigma^2))));

[gch, pixelLabels_hat] = GraphCut('swap',gch);
gch = GraphCut('close', gch);
pixelLabels_hat = pixelLabels_hat + 1;

% figure(1);
% %imagesc(pixelLabels_hat);
% stripedMask(im, im_sized_priors > 0);
% figure(2);
% stripedMask(im, pixelLabels_hat == 1);
% pause;

mask = pixelLabels_hat == 1;

% % for fairness of comparison
% mask2 = imresize(mask, size(prior_mask)) > 0.5;
% mask = mask2;

%predictLabel = labelSubset(pixelLabels_hat);

% toc_mrf = toc_mrf + toc;
% fprintf('Solving MRF in %f seconds\n', toc_mrf);