% Evaluating RBM by ICM (iterated conditional modes)

gt = cell(length(list),1);
y = cell(length(list),1);

for f = 1 : length(list)
    
    load([segpath list{f} '_t' num2str(threshold) '.mat']);
    load([feapath list{f} '.mat']);
    load([hogpath list{f} '.mat']);
    mask = imread([maskpath list{f} '.png']);
    mask = imresize(mask,[height, width]);
    gt{f}= uint8(mask(:)>0.5);
    
    predicthid = [hog(:); 1]'*feahid;
    
    ft = vl_homkermap([wordHist; colorHist; shapeHist], 1, 'Gamma', 0.7);
    feascore = feavis*[ft; ones(1,size(ft,2))];
    feascore = feascore(regions);
    predictvis = imresize(feascore, [height,width]);
    predictvis = predictvis(:)';
    
    probhid = 1 ./ (1 + exp(-predicthid));
    hid0 = (probhid > 0.5);

    % overall, hog + superpixel
    [vis, hid] = decode_blp(vishid, visbiases+predictvis, hidbiases+predicthid, 100, [], hid0);
    vis = reshape(vis,[height, width]);
    predictmask = uint8(vis > 0.5);
    y{f} = predictmask(:);
    
    foreground = find(gt{f}==1);
    background = find(gt{f}==0);

end

gt = cell2mat(gt);
y  = cell2mat(y);

foreground = find(gt==1);
background = find(gt==0);

foreaccuracy = sum(y(foreground)==1)/length(foreground);
backaccuracy = sum(y(background)==0)/length(background);
avg = (foreaccuracy + backaccuracy) / 2;

