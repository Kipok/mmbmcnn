function [vis, hid] = decode_blp(vishid, visbias, hidbias, BLPIters, vis0, hid0)

if ~isempty(hid0) && isempty(vis0)
    hid = hid0;
    % BLP iterations
%     options = optimset('Display', 'off');
    for i = 1 : BLPIters
        func = hid*vishid' + visbias;
%         [vis,fval] = linprog(double(func'),[],[],[],[],zeros(length(func),1),ones(length(func),1),[],options);
        vis = double(func > 0);
        fval = vis*func' + hidbias*hid';
        
        func_new = vis*vishid + hidbias;
%         [hid,fval_new] = linprog(double(func'),[],[],[],[],zeros(length(func),1),ones(length(func),1),[],options);
        hid = double(func_new > 0);
        fval_new = hid*func_new' + visbias*vis';
        if abs(fval-fval_new) < 1e-3
            break;
        end
    end
end

if isempty(hid0) && ~isempty(vis0)
    vis = vis0;
    % BLP iterations
%     options = optimset('Display', 'off');
    for i = 1 : BLPIters
        func = vis*vishid + hidbias;
        hid = double(func > 0);
%         [hid,fval_new] = linprog(double(func'),[],[],[],[],zeros(length(func),1),ones(length(func),1),[],options);
        fval = hid*func' + visbias*vis';
        
        func_new = hid*vishid' + visbias;
%         [vis,fval] = linprog(double(func'),[],[],[],[],zeros(length(func),1),ones(length(func),1),[],options);
        vis = double(func_new > 0);
        fval_new = vis*func_new' + hidbias*hid';
        if abs(fval-fval_new) < 1e-3
            break;
        end
    end
end

if isempty(hid0) && isempty(vis0)
    hid = rand(size(hidbias));
    % BLP iterations
%     options = optimset('Display', 'off');
    for i = 1 : BLPIters
        func = hid*vishid' + visbias;
%         [vis,fval] = linprog(double(func'),[],[],[],[],zeros(length(func),1),ones(length(func),1),[],options);
        vis = double(func > 0);
        fval = vis*func' + hidbias*hid';
        func_new = vis*vishid + hidbias;
%         [hid,fval_new] = linprog(double(func'),[],[],[],[],zeros(length(func),1),ones(length(func),1),[],options);
        hid = double(func_new > 0);
        fval_new = hid*func_new' + visbias*vis';
        if abs(fval-fval_new) < 1e-3
            break;
        end
    end
end