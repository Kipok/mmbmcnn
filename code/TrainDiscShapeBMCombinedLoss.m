% max margin training with stochastic gradient descent
% written by Jimei Yang @UCM, May 2013

load(modelstr_shapebm_l2);
hidpen = vishid;
penbiases = hidbiases;
visbiases_l2 = visbiases;

load(modelstr_shapebm_l1);
hidbiases = (hidbiases + visbiases_l2);


load(modelstr_hogvis);
load(modelstr_feahid2);

feapen = [feapen; feapenbiases];
feahid = [feahid; feahidbiases];
feavis = hogvis; 

% feapen = rand(size(feapen))-0.5;
% feahid = rand(size(feahid))-0.5;
% feavis = rand(size(feavis))-0.5;

save([modelpath_shapbm_combinedloss 'model_iter' num2str(0) '.mat'],'vishid', 'vishid_tilde', 'visbiases', 'hidbiases', ...
    'hidpen', 'penbiases', 'feahid', 'feavis', 'feapen');
fid = fopen('../data/shapebm_log.txt', 'w');

% parameters 
rho = 1e-5;
c = 1e-1;
CCCPIterations = iter_shapebm;
t0 = CCCPIterations/5;
t = 0;

% start training
for i = 1 : CCCPIterations
    
    tic;
    
    % Take a random permutation of the samples
    ind_rnd = randperm(length(trainlist));
    
    % update the learning rate
    t = t + 1;
    rho_i = min(rho, rho*t0/t);
    
    fprintf('Train Discriminative ShapeBM with Combined Loss,  Loop = %d ', i);
    fprintf(fid, 'Train Discriminative ShapeBM with Combined Loss,  Loop = %d ', i);

    for batch = 1 : length(trainlist)
        
        if mod(batch,100)==0
            fprintf('.');
        end
        
        bIdx = ind_rnd(batch);
        
        % load the data
        id = trainlist{bIdx};
        load([hogpath id '.mat']);
        load([partpath id '.mat']);
        load([feapath id '.mat']);
        load([segpath id '_t' num2str(threshold) '.mat']);
        gt = imread([maskpath id '.png']);
        gt = gt > 0;
        
        % compute the image dependent energy
        predictpen = [hog(:); 1]'*feapen;
        predicthid = zeros(size(hidbiases));
        for k = 1:4
            predicthid(hidindices(:,k)>0) = [hogparts(:,k);1]'*feahid(:,hidindices(:,k)>0);
        end
        ft = vl_homkermap([wordHist; colorHist; shapeHist], 1, 'Gamma', 0.7);
        feascore = feavis*[ft; ones(1,size(ft,2))];
        feascore = feascore(regions);
        predictvis = imresize(feascore, [height,width]);
        predictvis = predictvis(:)';
        
        % infer hidden states given visibles
        mask = imresize(gt, [height, width]);
        vis_star = double(mask(:)');
        [hid_star, pen_star] = decode_data_blp(vishid_tilde, visbiases, hidbiases, hidpen, penbiases, 100, vis_star);
        
        % loss augmented energy minimization
        penbiastotal = penbiases + predictpen;
        penbiastotal_augmented = penbiastotal + double(pen_star==0) - double(pen_star==1);
        hidbiastotal = hidbiases + predicthid;
        hidbiastotal_augmented = hidbiastotal + double(hid_star==0) - double(hid_star==1);
        visbiastotal = visbiases + predictvis;
        visbiastotal_augmented = visbiastotal + double(vis_star==0) - double(vis_star==1);
        [vis, hid, pen] = decode_model_blp(vishid_tilde, visbiases+predictvis, hidbiases+predicthid, ...
            hidpen, penbiases+predictpen, 100, []);
        
        % compute features for data part
        vishidinc_data = convertimage2patch(vis_star'*hid_star,visindices,hidindices);
        visbiasinc_data = vis_star;
        hidbiasinc_data = hid_star;
        hidpeninc_data = hid_star'*pen_star;
        penbiasinc_data = pen_star;

        feapeninc_data = [hog(:);1]*pen_star;
        feahidinc_data = zeros(size(feahid));
        for k = 1:4
            feahidinc_data(:,hidindices(:,k)>0) = [hogparts(:,k);1]*hid_star(hidindices(:,k)>0);
        end
        vissuper_star = imresize(reshape(vis_star,[height,width]),size(gt));
        visregion_star = zeros(size(ft,2),1);
        for r = 1 : length(unique(regions))
            s = vissuper_star(regions==r);
            visregion_star(r) = (mean(s)>0.5);
        end
        feavisinc_data = visregion_star'*[ft; ones(1,size(ft,2))]';
        
        % compute features for model part
        vishidinc_model = convertimage2patch(vis'*hid,visindices,hidindices);
        visbiasinc_model = vis;
        hidbiasinc_model = hid;
        hidpeninc_model = hid'*pen;
        penbiasinc_model = pen;

        feapeninc_model = [hog(:);1]*pen;
        feahidinc_model = zeros(size(feahid));
        for k = 1:4
            feahidinc_model(:,hidindices(:,k)>0) = [hogparts(:,k);1]*hid(hidindices(:,k)>0);
        end
        vissuper = imresize(reshape(vis,[height,width]),size(gt));
        visregion = zeros(size(ft,2),1);
        for r = 1 : length(unique(regions))
            s = vissuper(regions==r);
            visregion(r) = (mean(s)>0.5);
        end
        feavisinc_model = visregion'*[ft; ones(1,size(ft,2))]';
        
        % compute the gradients
        vishidinc = c*vishid + (vishidinc_model - vishidinc_data);
        visbiasinc = c*visbiases + (visbiasinc_model - visbiasinc_data);
        hidbiasinc = c*hidbiases + (hidbiasinc_model - hidbiasinc_data);
        hidpeninc = c*hidpen + (hidpeninc_model - hidpeninc_data);
        penbiasinc = c*penbiases + (penbiasinc_model - penbiasinc_data);
        feahidinc = c*feahid + (feahidinc_model - feahidinc_data);
        feavisinc = c*feavis + (feavisinc_model - feavisinc_data);
        feapeninc = c*feapen + (feapeninc_model - feapeninc_data);
    
        % update parameters
        vishid = vishid - rho_i*vishidinc;
        vishid_tilde = convertpatch2image(vishid,visindices,hidindices);
        visbiases = visbiases - rho_i*visbiasinc;
        hidbiases = hidbiases - rho_i*hidbiasinc;
        hidpen = hidpen - rho_i*hidpeninc;
        feahid = feahid - rho_i*feahidinc;
        feavis = feavis - rho_i*feavisinc;
        feapen = feapen - rho_i*feapeninc;

    end
    
    list = trainlist; EvaluateDiscShapeBM; trainAcc = avg;
    list = testlist; EvaluateDiscShapeBM; testAcc = avg;
    % Show progress in epoch
    fprintf(' TrainAcc = %f, TestAcc = %f, Elapsed Time = %f\n', trainAcc, testAcc, toc);
    fprintf(fid, ' TrainAcc = %f, TestAcc = %f, Elapsed Time = %f\n', trainAcc, testAcc, toc);

    if i == itershapebm
        save([modelpath_shapbm_combinedloss 'model_iter' num2str(i) '.mat'],'vishid', 'vishid_tilde', 'visbiases', 'hidbiases', ...
            'hidpen', 'penbiases', 'feahid', 'feavis', 'feapen');
    end
    
end
    
save(modelstr_mmbm2, 'vishid', 'vishid_tilde', 'visbiases', 'hidbiases', ...
        'hidpen', 'penbiases', 'feahid', 'feavis', 'feapen');
