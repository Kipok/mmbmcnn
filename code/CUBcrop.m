% preprocess caltech_ucsd birds
AddPath;

height = 128; width = 128;

datadir = '../data/Caltech-UCSD-Birds-200/images/';
annotdir = '../data/Caltech-UCSD-Birds-200/GroundTruth/';
list = textread('../data/Caltech-UCSD-Birds-200/lists/files.txt','%s');

imgdir = '../data/CUBdata/birdImgs/';
if ~isdir(imgdir)
    mkdir(imgdir);
end

maskdir = '../data/CUBdata/birdMasks/';
if ~isdir(maskdir)
    mkdir(maskdir)
end

for f = 1 : numel(list)
    f
    imgfile = [datadir list{f}];
    im = imread(imgfile);
    annotfile = [annotdir list{f}(1:end-4) '.png'];
    gt = imread(annotfile);
    xx = find(sum(gt,1)); 
    yy = find(sum(gt,2));
    bbox.left = xx(1); bbox.right = xx(end);
    bbox.top = yy(1); bbox.bottom = yy(end);
    
    im = im(bbox.top:bbox.bottom, bbox.left:bbox.right, :);
    seg = gt(bbox.top:bbox.bottom, bbox.left:bbox.right);
    im = imresize(im, [height, width]);
    seg = imresize(seg, [height, width]);
    
    [pathstr, name, ext] = fileparts(list{f});
    imgstr = [imgdir pathstr '/'];
    if ~isdir(imgstr)
        mkdir(imgstr);
    end
    imwrite(im,[imgstr name '.png'], 'png');
    
    maskstr = [maskdir pathstr '/'];
    if ~isdir(maskstr)
        mkdir(maskstr);
    end
    imwrite(seg,[maskstr name '.png'], 'png');
%     show_annotation(imgfile, annotfile);
%     pause;
end