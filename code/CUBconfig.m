% config UCSD-Caltech Bird 200 dataset

load('../data/CUBdata/list.mat');

imgpath = '../data/CUBdata/birdImgs/';
maskpath = '../data/CUBdata/birdMasks/';

% ------------------ features --------------------------
sz_sift = 512;
siftvocabpath = ['../data/CUBdata/siftVocab' num2str(sz_sift) '.mat'];

sz_color = 128;
colorvocabpath = ['../data/CUBdata/colorVocab' num2str(sz_color) '.mat'];

llcK = 5;

nLabels = 2;

threshold = 10;

cellSize = 8;

lambda = 50;

ucmpath = '../data/CUBdata/birdUCMs/';
if ~isdir(ucmpath)
    mkdir(ucmpath);
end

segpath = '../data/CUBdata/birdSegs/';
if ~isdir(segpath)
    mkdir(segpath)
end

feapath = '../data/CUBdata/birdFeatures/';
if ~isdir(feapath)
    mkdir(feapath)
end

hogpath = '../data/CUBdata/birdHOGs/';
if ~isdir(hogpath)
    mkdir(hogpath);
end

partpath = '../data/CUBdata/birdHOGParts/';
if ~isdir(partpath)
    mkdir(partpath);
end

% ------------- RBMs --------------------------
hmax = 128;
wmax = 128;
height = 64;
width = 64;
scaling = 2;
batchsize = 100;
stride=[28,28];
cellheight=36;
cellwidth=36;
numpatch=[2,2];
numhid=500; 
numpen=100;
PatternIndexing;

if ~isdir('../data/models/')
    mkdir('../data/models/')
end

modelstr_rbm = '../data/models/initcubvishid.mat';
modelstr_shapebm_l1 = '../data/models/initcubvishid_l1.mat';
modelstr_shapebm_l2 = '../data/models/initcubvishid_l2.mat';
modelstr_shapebm_mf = '../data/models/initcubvishid_mf.mat';

% ------------ MMBMs ---------------------------
modelstr_feavis = '../data/models/initcubfeavis.mat';
modelstr_hogvis = '../data/models/initcubhogvis.mat';
modelstr_feahid = '../data/models/initcubfeahid.mat';
modelstr_feahid2 = '../data/models/initcubfeahid2.mat';

hidpath = '../data/CUBdata/birdHiddens/';
if ~isdir(hidpath)
    mkdir(hidpath);
end

hid2path = '../data/CUBdata/birdHiddens2/';
if ~isdir(hid2path)
    mkdir(hid2path);
end

modelpath_combinedloss = '../data/CUBdata/birdModels_combinedLoss/';
if ~isdir(modelpath_combinedloss)
    mkdir(modelpath_combinedloss);
end
modelstr_mmbm1 = '../data/models/cubmodel_rbm.mat';

modelpath_shapbm_combinedloss = '../data/CUBdata/birdModels_ShapeBM_combinedLoss/';
if ~isdir(modelpath_shapbm_combinedloss)
    mkdir(modelpath_shapbm_combinedloss);
end
modelstr_mmbm2 = '../data/models/cubmodel_shapebm.mat';

respath_urbm = '../data/CUBdata/results/u_rbm/';
if ~isdir(respath_urbm)
    mkdir(respath_urbm);
end
respath_rbmgc = '../data/CUBdata/results/rbm_gc/';
if ~isdir(respath_rbmgc)
    mkdir(respath_rbmgc);
end
respath_ushapebm = '../data/CUBdata/results/u_shapebm/';
if ~isdir(respath_ushapebm)
    mkdir(respath_ushapebm);
end
respath_shapebmgc = '../data/CUBdata/results/shapebm_gc/';
if ~isdir(respath_shapebmgc)
    mkdir(respath_shapebmgc);
end