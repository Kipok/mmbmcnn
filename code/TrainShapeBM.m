% Train RBMs

randn('state',100);
rand('state',100);
warning off

% %%%%%% Training ShapeBM layer 1 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 [numcases numdims numbatches]=size(batchdata);
 maxepoch=1000;
 fprintf(1,'Pretraining Layer 1 with RBM: %d-%d \n',numdims,numhid);
 restart=1;
 ShapeBM_l1;
 save(modelstr_shapebm_l1, 'vishid', 'vishid_tilde', 'visbiases', 'hidbiases', 'epoch'); 


%%%%%% Training ShapeBM layer 2 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
maxepoch=1000;
fprintf(1,'Pretraining Layer 2 with RBM: %d-%d \n',numhid,numpen);
restart=1;
ShapeBM_l2;
save(modelstr_shapebm_l2, 'vishid', 'visbiases', 'hidbiases', 'epoch'); 


%%%%%% Training two-layer Boltzmann machine %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
fprintf(1,'Training a Shape Bolztamnn Machine. \n');
restart=1;
ShapeBM_MF;
save(modelstr_shapebm_mf, 'vishid', 'vishid_tilde', 'visbiases', 'hidbiases', 'hidpen', 'penbiases', 'epoch'); 




