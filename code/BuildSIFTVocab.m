% train sift vocabulary

list = trainlist;

N = length(list);
K = 100;

sample = vl_colsubset(1:numel(list),N);

all_sifts = zeros(128, N*K);

for i = 1:numel(sample)
   i
   im = imread([imgpath list{i} '.png']);
   [dummy sifts] = vl_phow(single(rgb2gray(im)),'Step',4);
   all_sifts(:,(i-1)*K+1:i*K) = vl_colsubset(sifts,K);   
end

[siftVocab ind] = vl_ikmeans(uint8(all_sifts),sz_sift);
save(siftvocabpath,'siftVocab');