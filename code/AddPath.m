% add all the paths
addpath(genpath('SegmentCode/'));
addpath(genpath('FeatureCode/'));
addpath(genpath('BMs'));
addpath(genpath('liblinear-1.8'));
addpath('graph-cut');
addpath('vlfeat-0.9.16');
vl_setup;