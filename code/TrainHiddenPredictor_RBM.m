% PFP pre-train hidfea
list = trainlist;

load([hidpath list{1} '.mat']);
load([hogpath list{1} '.mat']);
hog = hog(:);
    
data = zeros(length(list),length(hog));
label = zeros(length(list),length(hid));

for f = 1 : length(list)
    load([hidpath list{f} '.mat']);
    label(f,:) = double(hid);
    load([hogpath list{f} '.mat']);
    data(f,:) = hog(:);
end

feahid = zeros(size(data,2),length(hid));
feahidbiases = zeros(1,length(hid));
matlabpool 4
parfor c = 1 : length(hid)
    c
    model = train(label(:,c), sparse(data), '-s 0 -B 1 -q 1');
    predict(label(:,c), sparse(data), model);
    if model.Label(1)==0
        feahid(:,c) = -model.w(:,1:end-1)';
        feahidbiases(c) = -model.w(end);
    else
        feahid(:,c) = model.w(:,1:end-1)';
        feahidbiases(c) = model.w(end);
    end
    
    template = reshape(feahid(:,c),size(hog));
    imhog = vl_hog('render', single(template.*(template>0))) ;
%     clf ; imshow(imhog,[]) ; colormap gray ; drawnow;
end
matlabpool close
save(modelstr_feahid, 'feahid', 'feahidbiases');
