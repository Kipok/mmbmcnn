
gt = cell(length(list),1);
y = cell(length(list),1);

for f = 1 : length(list)
    
    load([segpath list{f} '_t' num2str(threshold) '.mat']);
    load([feapath list{f} '.mat']);
    load([hogpath list{f} '.mat']);
    load([partpath list{f} '.mat']);
    mask = imread([maskpath list{f} '.png']);
    mask = imresize(mask,[height, width]);
    gt{f}= uint8(mask(:)>0);
    
    predictpen = [hog(:); 1]'*feapen;
    
    predicthid = zeros(size(hidbiases));
    for k = 1:4
        predicthid(hidindices(:,k)>0) = [hogparts(:,k);1]'*feahid(:,hidindices(:,k)>0);
    end
    
    ft = vl_homkermap([wordHist; colorHist; shapeHist], 1, 'Gamma', 0.7);
    feascore = feavis*[ft; ones(1,size(ft,2))];
    feascore = feascore(regions);
    predictvis = imresize(feascore, [height,width]);
    predictvis = predictvis(:)';
    
    probhid = 1 ./ (1 + exp(-predicthid));
    hid0 = (probhid > 0.5);

    % overall, hog + superpixel
    [vis, hid, pen] = decode_model_blp(vishid_tilde, visbiases+predictvis, hidbiases+predicthid, ...
        hidpen, penbiases+predictpen, 100, hid0);
    vis = reshape(vis,[height, width]);
    predictmask = uint8(vis > 0.5);
    y{f} = predictmask(:);
    
end

gt = cell2mat(gt);
y  = cell2mat(y);

foreground = find(gt==1);
background = find(gt==0);

foreaccuracy = sum(y(foreground)==1)/length(foreground);
backaccuracy = sum(y(background)==0)/length(background);
avg = (foreaccuracy + backaccuracy) / 2;
tp = sum((gt==1) & (y==1));
fp = sum((gt~=1) & (y==1));
fn = sum((gt==1) & (y~=1));
tn = sum((gt~=1) & (y~=1));
overlap = tp / (tp+fp+fn);

