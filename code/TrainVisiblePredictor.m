% PFP pre-train visfea

list = trainlist;
    
nTrain = numel(list);
data = cell(1,nTrain);
label = cell(1,nTrain);
for f = 1:numel(list)
    load([feapath list{f} '.mat']);             
    ft = vl_homkermap([wordHist; colorHist; shapeHist], 1, 'Gamma', 0.7);
    posMask = labelHist(1,:)./sum(labelHist) > 0.95;
    negMask = labelHist(2,:)./sum(labelHist) > 0.95;
    if sum(posMask) < sum(negMask)
        positive = ft(:,posMask);
        negative = vl_colsubset(ft(:,negMask), sum(posMask));
    else
        negative = ft(:,negMask);
        positive = vl_colsubset(ft(:,posMask), sum(negMask));
    end
    data{f} = [positive negative];
    label{f} = [ones(1, size(positive,2),'int8'), -ones(1,size(negative,2),'int8')];
end

data = cell2mat(data);
label= cell2mat(label);
model = train(double(label'), sparse(double(data')),'-B 1');
predict(double(label'),sparse(double(data')),model);

feavis = model.w(1:end-1);
feavisbias = model.w(end);
save(modelstr_feavis, 'feavis', 'feavisbias');

