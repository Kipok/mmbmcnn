% Infer hidden states

list = trainlist;
load(modelstr_shapebm_mf);

for f = 1 : length(list)
    f
    mask = imread([maskpath list{f} '.png']);
    mask = imresize(mask, [height, width]);
    data = double(mask(:)'>0);
    [hid,pen] = decode_data_blp(vishid_tilde, visbiases, hidbiases,hidpen,penbiases, 100, data);
    
    [pathstr, name, ext] = fileparts(list{f});
    hid2str = [hid2path pathstr '/'];
    if ~isdir(hid2str)
        mkdir(hid2str);
    end
    
    save([hid2path list{f} '.mat'], 'hid','pen');
end

