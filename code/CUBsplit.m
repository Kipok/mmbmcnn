% preprocess caltech_ucsd birds
AddPath;

list = textread('../data/Caltech-UCSD-Birds-200/lists/files.txt','%s');
imglist = cell(size(list));
for f = 1 : numel(list)
    [pathstr, name, ext] = fileparts(list{f});
    imglist{f} = [pathstr '/' name];
end

list = textread('../data/Caltech-UCSD-Birds-200/lists/train.txt','%s');
trainlist = cell(size(list));
for f = 1 : numel(list)
    [pathstr, name, ext] = fileparts(list{f});
    trainlist{f} = [pathstr '/' name];
end

list = textread('../data/Caltech-UCSD-Birds-200/lists/test.txt','%s');
testlist = cell(size(list));
for f = 1 : numel(list)
    [pathstr, name, ext] = fileparts(list{f});
    testlist{f} = [pathstr '/' name];
end

save('../data/CUBdata/list.mat', 'imglist', 'trainlist', 'testlist');