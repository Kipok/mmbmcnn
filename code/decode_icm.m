function [vis, hid] = decode_icm(vishid, visbias, hidbias, ICMIters, vis0, hid0)

if isempty(hid0) && ~isempty(vis0)
    vis = vis0;
    % ICM iterations
    for i = 1 : ICMIters
        probhid = 1 ./ (1 + exp(- vis*vishid - hidbias));
        hid = double(probhid > 0.5);
        probvis = 1 ./ (1 + exp(- hid*vishid' - visbias));
        vis = double(probvis > 0.5);
    end
end

if ~isempty(hid0) && isempty(vis0)
    hid = hid0;
    % ICM iterations
    for i = 1 : ICMIters
        probvis = 1 ./ (1 + exp(- hid*vishid' - visbias));
        vis = double(probvis > 0.5);
        probhid = 1 ./ (1 + exp(- vis*vishid - hidbias));
        hid = double(probhid > 0.5);
    end
end

if isempty(hid0) && isempty(vis0)
    hid = (rand(size(hidbias)) > 0.5);
    % ICM iterations
    for i = 1 : ICMIters
        probvis = 1 ./ (1 + exp(- hid*vishid' - visbias));
        vis = double(probvis > 0.5);
        probhid = 1 ./ (1 + exp(- vis*vishid - hidbias));
        hid = double(probhid > 0.5);
    end
end